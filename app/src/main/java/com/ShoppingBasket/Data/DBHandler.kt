package com.ShoppingBasket.Data

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.content.Context
import android.database.Cursor
import android.util.Log
import com.example.android.shoppingbasket.Entities.*
import com.example.android.shoppingbasket.Entities.enums.CategoryEnum
import com.example.android.shoppingbasket.Entities.enums.Retail

class DBHandler(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {


    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_LOCATION_TABLE)
        Log.i("Sql Query: ", CREATE_LOCATION_TABLE)
        db.execSQL(CREATE_OPENING_HOURS_TABLE)
        Log.i("Sql Query: ", CREATE_OPENING_HOURS_TABLE)
        db.execSQL(CREATE_RETAIL_CHAINS_TABLE)
        Log.i("Sql Query: ", CREATE_RETAIL_CHAINS_TABLE)
        db.execSQL(CREATE_SHOP_ITEMS_TABLE)
        Log.i("Sql Query: ", CREATE_SHOP_ITEMS_TABLE)
        db.execSQL(CREATE_STORE_TABLE)
        Log.i("Sql Query: ", CREATE_STORE_TABLE)
        db.execSQL(CREATE_CART_TABLE)
        Log.i("Sql Query: ", CREATE_CART_TABLE)
        db.execSQL(CREATE_COUPON_TABLE)
        Log.i("Sql Query: ", CREATE_COUPON_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS $TABLE_STORE")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_SHOP_ITEM")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_RETAIL_CHAIN")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_OPENING_HOURS")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_LOCATION")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_CART")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_COUPON")

        // create new tables
        onCreate(db)
    }

    fun resetDatabase() {
        val db = this.writableDatabase
        db.execSQL("DROP TABLE IF EXISTS $TABLE_STORE")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_SHOP_ITEM")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_RETAIL_CHAIN")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_OPENING_HOURS")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_LOCATION")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_CART")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_COUPON")

        // create new tables
        db.execSQL(CREATE_LOCATION_TABLE)
        Log.e("Sql Query: ", CREATE_LOCATION_TABLE)
        db.execSQL(CREATE_OPENING_HOURS_TABLE)
        Log.e("Sql Query: ", CREATE_OPENING_HOURS_TABLE)
        db.execSQL(CREATE_RETAIL_CHAINS_TABLE)
        Log.e("Sql Query: ", CREATE_RETAIL_CHAINS_TABLE)
        db.execSQL(CREATE_SHOP_ITEMS_TABLE)
        Log.e("Sql Query: ", CREATE_SHOP_ITEMS_TABLE)
        db.execSQL(CREATE_STORE_TABLE)
        Log.e("Sql Query: ", CREATE_STORE_TABLE)
        db.execSQL(CREATE_CART_TABLE)
        Log.e("Sql Query: ", CREATE_CART_TABLE)
        db.execSQL(CREATE_COUPON_TABLE)
        Log.e("Sql Query: ", CREATE_COUPON_TABLE)
    }

    fun closeDB() {
        val db = this.readableDatabase
        if (db != null && db.isOpen)
            db.close()
    }


    fun getAllEntries(obj: Any): List<Any> {
        val selectQuery: String
        val list: List<Any>?

        when(obj) {
            is Location -> {
                list = ArrayList<Location>()
                selectQuery = "SELECT * FROM $TABLE_LOCATION"
                Log.e("SelectQuery: ", selectQuery)

                val db: SQLiteDatabase  = this.readableDatabase
                val c: Cursor = db.rawQuery(selectQuery, null)
                if(c.moveToFirst() && c.count > 0) {
                    do {
                        list.add(fetchEntriesByType(obj, c) as Location)

                    } while (c.moveToNext())
                }
                c.close()
                return list
            }
            is OpeningHours -> {
                list = ArrayList<OpeningHours>()
                selectQuery = "SELECT * FROM $TABLE_OPENING_HOURS"
                Log.e("SelectQuery: ", selectQuery)

                val db: SQLiteDatabase  = this.readableDatabase
                val c: Cursor = db.rawQuery(selectQuery, null)
                if(c.moveToFirst() && c.count > 0) {
                    do {
                        list.add(fetchEntriesByType(obj, c) as OpeningHours)

                    } while (c.moveToNext())
                }
                c.close()
                return list
            }
            is RetailChain -> {
                list = ArrayList<RetailChain>()
                selectQuery = "SELECT * FROM $TABLE_RETAIL_CHAIN"
                Log.e("SelectQuery: ", selectQuery)

                var db: SQLiteDatabase  = this.readableDatabase
                var c: Cursor = db.rawQuery(selectQuery, null)
                if(c.moveToFirst() && c.count > 0) {
                    do {
                        list.add(fetchEntriesByType(obj, c) as RetailChain)

                    } while (c.moveToNext())
                }
                c.close()
                return list
            }
            is ShopItem -> {
                list = ArrayList<ShopItem>()
                selectQuery = "SELECT * FROM $TABLE_SHOP_ITEM"
                Log.e("SelectQuery: ", selectQuery)

                var db: SQLiteDatabase  = this.readableDatabase
                var c: Cursor = db.rawQuery(selectQuery, null)
                if(c.moveToFirst() && c.count > 0) {
                    do {
                        list.add(fetchEntriesByType(obj, c) as ShopItem)

                    } while (c.moveToNext())
                }
                c.close()
                return list
            }
            is Store -> {
                list = ArrayList<Store>()
                selectQuery = "SELECT * FROM $TABLE_STORE"
                Log.e("SelectQuery: ", selectQuery)

                val db: SQLiteDatabase  = this.readableDatabase
                val c: Cursor = db.rawQuery(selectQuery, null)
                if(c.moveToFirst() && c.count > 0) {
                    do {
                        list.add(fetchEntriesByType(obj, c) as Store)

                    } while (c.moveToNext())
                }
                c.close()
                return list
            }
            is Coupon -> {
                list = ArrayList<Coupon>()
                selectQuery = "SELECT * FROM $TABLE_COUPON"
                Log.e("SelectQuery: ", selectQuery)
                val db: SQLiteDatabase  = this.readableDatabase
                val c: Cursor = db.rawQuery(selectQuery, null)
                if(c.moveToFirst() && c.count > 0) {
                    do {
                        list.add(fetchEntriesByType(obj, c) as Coupon)

                    } while (c.moveToNext())
                }
                c.close()
                return list
            }
            else -> {
                list = ArrayList()
                Log.e("Error","Error in getAllEntries object type unknown ")
                return list
            }
        }
    }

    fun getAllItemsFromCart(): List<ShopItem> {
        var db: SQLiteDatabase  = this.readableDatabase
        var list = ArrayList<ShopItem>()
        var indexes = ArrayList<Int>()

        val selectQuery = "SELECT $KEY_ITEM_ID FROM $TABLE_CART"
        val c: Cursor = db.rawQuery(selectQuery, null)
        if(c.moveToFirst()) {
            do {
                indexes.add(c.getInt(c.getColumnIndex(KEY_ITEM_ID)))
            } while (c.moveToNext())
            for(i in indexes) {
                val temp = getEntry(i, ShopItem("")) as ShopItem?
                if (temp != null) {
                    list.add(temp)
                }
            }
        }
        c.close()
        return list
    }

    fun getItemsForCategory(category: CategoryEnum): List<ShopItem> {
        var db: SQLiteDatabase  = this.readableDatabase
        var list = ArrayList<ShopItem>()
        var indexes = ArrayList<Int>()

        val selectQuery = "SELECT $KEY_ID FROM $TABLE_SHOP_ITEM WHERE $KEY_CATEGORY = '${category.name}'"
        val c: Cursor = db.rawQuery(selectQuery, null)
        if(c.moveToFirst()) {
            do {
                indexes.add(c.getInt(c.getColumnIndex(KEY_ID)))
            } while (c.moveToNext())
            for(i in indexes) {
                val temp = getEntry(i, ShopItem("")) as ShopItem?
                if ((temp != null)) {
                    list.add(temp)
                }
            }
        }
        c.close()
        return list
    }

    fun getAllCouponsFromCart(): List<Coupon> {
        var db: SQLiteDatabase  = this.readableDatabase
        var list = ArrayList<Coupon>()
        var indexes = ArrayList<Int>()

        val selectQuery = "SELECT $KEY_COUPON_ID FROM $TABLE_CART"
        val c: Cursor = db.rawQuery(selectQuery, null)
        if(c.moveToFirst()) {
            do {
                indexes.add(c.getInt(c.getColumnIndex(KEY_COUPON_ID)))
            } while (c.moveToNext())
            for(i in indexes) {
                val temp = getEntry(i, Coupon()) as Coupon?
                 if ((temp != null)) {
                     list.add(temp)
                 }
            }
        }
        c.close()
        return list
    }

    fun getCursorForRetail(): Cursor? {
        var db: SQLiteDatabase = this.readableDatabase
        val queryCols = arrayOf( "$KEY_NAME _id")
        return db.query(true, TABLE_RETAIL_CHAIN, queryCols,null, null, null, null, null, null)
    }

    fun putItemInCart(id: Int) {
        var db: SQLiteDatabase  = this.writableDatabase
        var values = ContentValues()
        values.put(KEY_ITEM_ID, id)
        db.insert(TABLE_CART, null, values)
    }

    fun putCouponInCart(id: Int) {
        var db: SQLiteDatabase  = this.writableDatabase
        var values = ContentValues()
        values.put(KEY_COUPON_ID, id)
        db.insert(TABLE_CART, null, values)
    }

    fun deleteItemFromCart(id: Int) {
        var db: SQLiteDatabase  = this.writableDatabase
        db.delete(TABLE_CART, "$KEY_ITEM_ID = ?",  Array(1){id.toString()})
    }

    fun deleteCouponFromCart(id: Int) {
        var db: SQLiteDatabase  = this.writableDatabase
        db.execSQL("DELETE FROM $TABLE_CART WHERE $KEY_COUPON_ID = $id")
        //For some reason below statement does not delete the entry, has to look into it.
//        db.delete(TABLE_CART, "$KEY_COUPON_ID = ?",  Array(1){id.toString()})
    }

    fun getEntry(id: Int, obj: Any): Any? {
        var db: SQLiteDatabase = this.readableDatabase
        val tableName: String = when(obj) {
            is Location -> TABLE_LOCATION
            is OpeningHours -> TABLE_OPENING_HOURS
            is RetailChain -> TABLE_RETAIL_CHAIN
            is ShopItem -> TABLE_SHOP_ITEM
            is Store -> TABLE_STORE
            is Coupon -> TABLE_COUPON
            else -> "empty"
        }

        val selectQuery = "SELECT * FROM $tableName WHERE $KEY_ID='$id'"
        Log.e("getEntry Query: ", selectQuery)

        val c: Cursor = db.rawQuery(selectQuery, null)
        if(c.moveToFirst() && c.count > 0) {
            return fetchEntriesByType(obj, c)
        }
        return null
    }

    fun getEntryByBarcode(code: String): ShopItem? {
        val db: SQLiteDatabase = this.readableDatabase
        val selectQuery = "SELECT * FROM $TABLE_SHOP_ITEM WHERE $KEY_BARCODE LIKE '$code'"
        val c = db.rawQuery(selectQuery, null)
        if (c == null || c.count <= 0) return null
        val result = ShopItem("")
        result.id = c.getInt(c.getColumnIndex(KEY_ID))
        result.name = c.getString(c.getColumnIndex(KEY_NAME))
        result.barCode = c.getString(c.getColumnIndex(KEY_BARCODE))
        result.category
        result.price = c.getFloat(c.getColumnIndex(KEY_PRICE))
        val temp = Store()
        result.shop = getEntry(c.getInt(c.getColumnIndex(KEY_SHOP)), temp) as Store?
        result.quantity = c.getString(c.getColumnIndex(KEY_QUANTITY))
        c.close()
        return result
    }

    fun updateEntry(obj: Any) {
        val db: SQLiteDatabase = this.writableDatabase
        updateOrCreate(db, obj, false)
    }

    fun createEntry(obj: Any): Int {
        val db: SQLiteDatabase = this.writableDatabase
        return updateOrCreate(db, obj, true)
    }

    fun deleteEntry(id: Int, obj: Any) {
        val db: SQLiteDatabase = this.writableDatabase

        when(obj) {
            is Location -> db.delete(TABLE_LOCATION, "$KEY_ID = ?",  Array<String>(1){obj.id.toString()})
            is OpeningHours -> db.delete(TABLE_OPENING_HOURS, "$KEY_ID = ?",  Array<String>(1){obj.id.toString()})
            is RetailChain -> db.delete(TABLE_RETAIL_CHAIN, "$KEY_ID = ?",  Array<String>(1){obj.id.toString()})
            is ShopItem -> db.delete(TABLE_SHOP_ITEM, "$KEY_ID = ?",  Array<String>(1){obj.id.toString()})
            is Store -> db.delete(TABLE_STORE, "$KEY_ID = ?",  Array<String>(1){obj.id.toString()})
            else -> Log.e("Error", "Unknown Object!")
        }
    }

    fun updateOrCreate(db : SQLiteDatabase, obj: Any, create: Boolean): Int {
        var values: ContentValues
        when (obj) {
            is Location -> {
                values = ContentValues()
                values.put(KEY_ADDRESS, obj.address)
                values.put(KEY_POST_CODE, obj.postCode)
                if(create) return (db.insert(TABLE_LOCATION, null, values)).toInt()
                    else db.update(TABLE_LOCATION, values, "$KEY_ID =?", Array(1){obj.id.toString()})
            }
            is OpeningHours -> {
                values = ContentValues()
                values.put(KEY_DAY, obj.day)
                values.put(KEY_OPEN_TIME, obj.openTime)
                values.put(KEY_CLOSE_TIME, obj.closeTime)
                if(create) return (db.insert(TABLE_OPENING_HOURS, null, values)).toInt()
                    else db.update(TABLE_OPENING_HOURS, values, "$KEY_ID =?", Array(1){obj.id.toString()})
            }
            is RetailChain -> {
                values = ContentValues()
                values.put(KEY_NAME, obj.name.toString())
                values.put(KEY_URL, obj.url)
                values.put(KEY_HOME_DELIVERY, obj.homeDelivery)
                if(create) return (db.insert(TABLE_RETAIL_CHAIN, null, values)).toInt()
                    else db.update(TABLE_RETAIL_CHAIN, values, "$KEY_ID =?", Array(1){obj.id.toString()})
            }
            is ShopItem -> {
                values = ContentValues()
                values.put(KEY_NAME, obj.name)
                values.put(KEY_BARCODE, obj.barCode)
                values.put(KEY_CATEGORY, obj.category)
                values.put(KEY_PRICE, obj.price)
                values.put(KEY_QUANTITY, obj.quantity)
                values.put(KEY_SHOP, obj.shop?.id)
                values.put(KEY_QUANTITY_NUMER, obj.number)

                if(create) return (db.insert(TABLE_SHOP_ITEM, null, values)).toInt()
                    else db.update(TABLE_SHOP_ITEM, values, "$KEY_ID =?", Array(1){obj.id.toString()})

            }
            is Store -> {
                values = ContentValues()
                values.put(KEY_NAME, obj.name)
                values.put(KEY_OPENING_HOURS, obj.openingHours?.id)
                values.put(KEY_LOCATION, obj.location?.id)
                values.put(KEY_RETAIL_CHAIN, obj.retailChain?.id)

                if(create) return (db.insert(TABLE_STORE, null, values)).toInt()
                    else db.update(TABLE_STORE, values, "$KEY_ID =?", Array(1){obj.id.toString()})
            }
            is Coupon -> {
                values = ContentValues()
                values.put(KEY_NAME, obj.name)
                values.put(KEY_MINIMUM_SPENDING, obj.ifMinimumSpending)
                values.put(KEY_DEDUCT_PERCENT, obj.deductPercent)
                values.put(KEY_CATEGORY, obj.forCategory)
                values.put(KEY_RETAIL_CHAIN, obj.forRetailChain?.id)

                if(create) return (db.insert(TABLE_COUPON, null, values)).toInt()
                    else db.update(TABLE_COUPON, values, "$KEY_ID =?", Array(1){obj.id.toString()})

            }
        }
        return -1
    }

    fun fetchEntriesByType(obj: Any,c: Cursor): Any? {
        var result: Any? = null
        when (obj) {
            is Location -> {
                result = Location(c.getString(c.getColumnIndex(KEY_ADDRESS)))
                result.id = c.getInt(c.getColumnIndex(KEY_ID))
                result.postCode = c.getString(c.getColumnIndex(KEY_POST_CODE))
            }
            is OpeningHours -> {
                result = OpeningHours()
                result.id = c.getInt(c.getColumnIndex(KEY_ID))
                result.day = c.getString(c.getColumnIndex(KEY_DAY))
                result.openTime = c.getString(c.getColumnIndex(KEY_OPEN_TIME))
                result.closeTime = c.getString(c.getColumnIndex(KEY_CLOSE_TIME))
            }
            is RetailChain -> {
                result = RetailChain(Retail.valueOf(c.getString(c.getColumnIndex(KEY_NAME))))
                result.id = c.getInt(c.getColumnIndex(KEY_ID))
                result.url = c.getString(c.getColumnIndex(KEY_URL))
                result.homeDelivery = c.getString(c.getColumnIndex(KEY_HOME_DELIVERY))
            }
            is ShopItem -> {
                result = ShopItem("")
                result.id = c.getInt(c.getColumnIndex(KEY_ID))
                result.name = c.getString(c.getColumnIndex(KEY_NAME))
                result.barCode = c.getString(c.getColumnIndex(KEY_BARCODE))
                result.category = c.getString(c.getColumnIndex(KEY_CATEGORY))
                result.price = c.getFloat(c.getColumnIndex(KEY_PRICE))
                result.shop = getEntry(c.getInt(c.getColumnIndex(KEY_SHOP)), Store()) as Store?
                result.quantity = c.getString(c.getColumnIndex(KEY_QUANTITY))

            }
            is Store -> {
                result = Store()
                result.name = c.getString(c.getColumnIndex(KEY_NAME))
                result.id = c.getInt(c.getColumnIndex(KEY_ID))
                val op = OpeningHours()
                result.openingHours = getEntry(c.getInt(c.getColumnIndex(KEY_OPENING_HOURS)), op) as OpeningHours?
                val loc = Location("")
                result.location = getEntry(c.getInt(c.getColumnIndex(KEY_LOCATION)), loc) as Location?
                val rc = RetailChain()
                result.retailChain = getEntry(c.getInt(c.getColumnIndex(KEY_RETAIL_CHAIN)), rc) as RetailChain?
            }
            is Coupon -> {
                result = Coupon()
                result.name = c.getString(c.getColumnIndex(KEY_NAME))
                result.id = c.getInt(c.getColumnIndex(KEY_ID))
                result.deductPercent = c.getInt(c.getColumnIndex(KEY_DEDUCT_PERCENT))
                result.ifMinimumSpending = c.getInt(c.getColumnIndex(KEY_MINIMUM_SPENDING))
                result.forCategory  = c.getString(c.getColumnIndex(KEY_CATEGORY))
                val rc = RetailChain()
                result.forRetailChain = getEntry(c.getInt(c.getColumnIndex(KEY_RETAIL_CHAIN)), rc) as RetailChain?
            }
            else -> {
                result = null
            }
        }
        return result
    }

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "shoppingDB.db"

        //Table names
        const val TABLE_LOCATION = "locations"
        const val TABLE_OPENING_HOURS = "opening_hours"
        const val TABLE_RETAIL_CHAIN = "retail_chain"
        const val TABLE_STORE = "stores"
        const val TABLE_SHOP_ITEM = "shop_items"
        const val TABLE_CART = "cart"
        const val TABLE_COUPON = "coupon"

        //Common column names
        const val KEY_ID = "id"
        const val KEY_NAME = "name"
        const val KEY_STORE_ID = "store_id"

        //Cart - columns
        const val KEY_ITEM_ID = "item_id"
        const val KEY_COUPON_ID = "coupon_id"

        //Locations Table - columns
        const val KEY_ADDRESS = "address"
        const val KEY_POST_CODE = "postCode"

        //Opening_Hours Table - columns
        const val KEY_DAY = "day"
        const val KEY_OPEN_TIME = "openTime"
        const val KEY_CLOSE_TIME = "closeTime"

        //Retail_Chains Table - columns
        const val KEY_URL = "url"
        const val KEY_HOME_DELIVERY = "homeDelivery"

        //Shop_Items Table - columns
        const val KEY_BARCODE = "barCode"
        const val KEY_CATEGORY = "category"
        const val KEY_PRICE = "price"
        const val KEY_SHOP = "shop"
        const val KEY_QUANTITY = "quantity"
        const val KEY_QUANTITY_NUMER = "quantity_number"

        //Stores Table - columns
        const val KEY_OPENING_HOURS = "openingHours"
        const val KEY_RETAIL_CHAIN = "retailChain"
        const val KEY_LOCATION = "location"

        //Coupon Table - columns
        const val KEY_DEDUCT_PERCENT = "deduct_percent"
        const val KEY_MINIMUM_SPENDING = "minimum_spending"

        //Table Creates
        private const val CREATE_COUPON_TABLE = ("CREATE TABLE $TABLE_COUPON ( $KEY_ID" +
                " INTEGER PRIMARY KEY, $KEY_NAME TEXT UNIQUE, $KEY_DEDUCT_PERCENT INTEGER," +
                " $KEY_MINIMUM_SPENDING INTEGER, $KEY_CATEGORY TEXT, $KEY_RETAIL_CHAIN INTEGER)")

        private const val CREATE_CART_TABLE = ("CREATE TABLE $TABLE_CART ( $KEY_ID" +
                " INTEGER PRIMARY KEY, $KEY_ITEM_ID INTEGER, $KEY_COUPON_ID)")

        private const val CREATE_SHOP_ITEMS_TABLE = ("CREATE TABLE $TABLE_SHOP_ITEM (" +
                "$KEY_ID INTEGER PRIMARY KEY AUTOINCREMENT, $KEY_NAME TEXT UNIQUE, $KEY_BARCODE TEXT UNIQUE," +
                " $KEY_CATEGORY TEXT, $KEY_PRICE REAL, $KEY_SHOP INTEGER, $KEY_QUANTITY TEXT, $KEY_QUANTITY_NUMER INTEGER)")

        private const val CREATE_LOCATION_TABLE = ("CREATE TABLE $TABLE_LOCATION (" +
                "$KEY_ID INTEGER PRIMARY KEY, $KEY_ADDRESS TEXT,$KEY_POST_CODE TEXT)")

        private const val CREATE_OPENING_HOURS_TABLE = ("CREATE TABLE $TABLE_OPENING_HOURS (" +
                " $KEY_ID INTEGER PRIMARY KEY, $KEY_DAY TEXT, $KEY_OPEN_TIME INTEGER, " +
                " $KEY_CLOSE_TIME INTEGER)")

        private const val CREATE_RETAIL_CHAINS_TABLE = ("CREATE TABLE $TABLE_RETAIL_CHAIN (" +
                " $KEY_ID INTEGER PRIMARY KEY, $KEY_NAME TEXT UNIQUE, $KEY_URL TEXT," +
                " $KEY_HOME_DELIVERY TEXT)")

        private const val CREATE_STORE_TABLE = ("CREATE TABLE $TABLE_STORE ($KEY_ID" +
                " INTEGER PRIMARY KEY, $KEY_NAME TEXT UNIQUE, $KEY_OPENING_HOURS INTEGER, $KEY_RETAIL_CHAIN INTEGER, $KEY_LOCATION INTEGER)")
    }

}
