package com.example.android.shoppingbasket

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.View
import android.widget.*
import com.ShoppingBasket.Data.DBHandler
import com.example.android.shoppingbasket.Entities.Cart
import com.example.android.shoppingbasket.Entities.Coupon
import com.example.android.shoppingbasket.Entities.Store
import java.io.Serializable

class CouponPicker : AppCompatActivity() {

    private val COUPON_REQUEST = 500
    var shoppingDb: DBHandler = DBHandler(this)
    private var cart: Cart = Cart.getCart()

    private lateinit var couponListView: ListView
    private lateinit var newCouponFAB: FloatingActionButton
    private lateinit var homeBTN: ImageButton
    private lateinit var counter : TextView


    lateinit var couponAdapter : ArrayAdapter<Coupon>
    lateinit var couponList: ArrayList<Coupon>
    var selectedCoupon: Coupon? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coupon_picker)

        val i = intent
        val extras = intent.extras

        homeBTN = findViewById(R.id.homeBTN)
        homeBTN.setOnClickListener {
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }

        counter = findViewById(R.id.Counter)
        counter.text = "${cart.items!!.size} items"

        newCouponFAB = findViewById(R.id.newCouponFAB)
        newCouponFAB.setOnClickListener {
            val toNewCoupon = Intent(this, newCoupon::class.java)
            startActivityForResult(toNewCoupon, COUPON_REQUEST)
        }

        couponList = shoppingDb.getAllEntries(Coupon()) as ArrayList<Coupon>
        couponListView = this.findViewById(R.id.couponList)
        couponAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, couponList)
//        couponAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        couponListView!!.adapter = couponAdapter

        class couponListActivity : Activity(), AdapterView.OnItemClickListener {
            override fun onItemClick(adapter: AdapterView<*>?, v: View?, position: Int, a: Long) {
                selectedCoupon = adapter!!.getItemAtPosition(position) as Coupon
                returnToList()

            }

        }
        couponListView.onItemClickListener = couponListActivity()
    }

    fun returnToList() {
        var data = Intent()
        var bundle = Bundle()
        bundle.putSerializable("coupon", selectedCoupon as Serializable)
        data.putExtras(bundle)
        setResult(Activity.RESULT_OK, data)
        finish()
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        when(requestCode) {
            COUPON_REQUEST -> {
                val result = intent!!.getSerializableExtra("coupon") as Coupon
                couponList.add(result)
                couponAdapter.notifyDataSetChanged()
//                shopSpinner!!.setSelection(shopAdapter.getPosition(result))
            }
            else -> {
                val toast = Toast.makeText(applicationContext,
                        "Unidentified request code!", Toast.LENGTH_SHORT)
                toast.show()
            }
        }
    }
}
