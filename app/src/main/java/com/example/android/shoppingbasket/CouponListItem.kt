package com.example.android.shoppingbasket


import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.support.v4.widget.DrawerLayout
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.android.shoppingbasket.Entities.Cart
import com.example.android.shoppingbasket.Entities.Coupon
import com.example.android.shoppingbasket.Entities.ShopItem
import com.example.android.shoppingbasket.Entities.enums.CategoryEnum

import java.util.ArrayList

/**
 * Created by Erik on 2017. 10. 08..
 */

class CouponListItem(_Cart_coupons: ArrayList<Coupon>, context: Context) : ArrayAdapter<Coupon>(context, R.layout.row_item, _Cart_coupons), View.OnClickListener {
    private val activity: Activity? = null
    private var cartCoupons: ArrayList<Coupon>? = null
    private var cart = Cart.getCart()
    internal var mContext: Context = context

    private var lastPosition = -1

    class ViewHolder {
        internal var txtName: TextView? = null
        internal var txtForCat: TextView? = null
        internal var txtPercentage: TextView? = null
        internal var txtMinimumSpending: TextView? = null
        internal var deleteBtn: ImageButton? = null
        internal var editBtn: ImageButton? = null
        internal var drawerLayout: DrawerLayout? = null
    }

    init {
        this.cartCoupons = _Cart_coupons
        inflater = LayoutInflater.from(context)
    }


    override fun getCount(): Int {
        return cart.coupons!!.size
    }

    override fun getItem(position: Int): Coupon? {
        return cart.coupons!![position]
    }

    fun setItem(position: Int, _Cart_coupons: Coupon) {
        cart.coupons!![position] = _Cart_coupons
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onClick(v: View) {

        val position = v.tag as Int
        val `object` = getItem(position)
        val dataModel = `object`
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        // Get the data item for this position
        val dataModel = cart.coupons!![position]
        // Check if an existing view is being reused, otherwise inflate the view
        val viewHolder = ViewHolder() // view lookup cache stored in tag

        val inflater = LayoutInflater.from(context)
        convertView = inflater.inflate(R.layout.coupon_item, parent, false)
        viewHolder.drawerLayout = convertView!!.findViewById(R.id.drawerLayout)
        viewHolder.drawerLayout!!.setScrimColor(Color.TRANSPARENT)

        viewHolder.txtName = convertView.findViewById(R.id.name)
        viewHolder.txtName!!.text = dataModel.name

        viewHolder.txtForCat = convertView.findViewById(R.id.catTxt)

        viewHolder.txtPercentage = convertView.findViewById(R.id.percTxt)

        viewHolder.txtMinimumSpending = convertView.findViewById(R.id.minTxt)

        viewHolder.deleteBtn = convertView.findViewById(R.id.deleteBtn)
        viewHolder.deleteBtn!!.setOnClickListener {
            if(cartCoupons!!.size >0) {
                cart.deleteCouponFromCart(getItem(position)!!.id)
                this.notifyDataSetChanged()
                if(mContext is BasketList) {
                    (mContext as BasketList).displayPrice()
                }
            }
        }

        viewHolder.editBtn = convertView.findViewById(R.id.editBtn)
        viewHolder.editBtn!!.setOnClickListener {
            if(cartCoupons!!.size > 0) {
                (mContext as BasketList).editCoupon(dataModel.id)
            }
        }

        convertView.tag = viewHolder

        if(dataModel.forCategory != null) {
            val cat : CategoryEnum = CategoryEnum.valueOf(dataModel.forCategory!!)
            viewHolder.txtForCat!!.text = CategoryEnum.valueOf(dataModel.forCategory.toString()).value
        }
        viewHolder.txtPercentage!!.text = "${dataModel.deductPercent}%"
        viewHolder.txtMinimumSpending!!.text = dataModel.ifMinimumSpending.toString()
        lastPosition = position
        // Return the completed view to render on screen
        return convertView
    }


    companion object {
        private var inflater: LayoutInflater? = null
    }

}
