package com.example.android.shoppingbasket.Entities

import java.io.Serializable

class OpeningHours : Serializable {
    var id: Int = 0
    var day: String = ""
    var openTime: String = "0"
    var closeTime: String = "24"

    fun OpeningHours(openTime: String, closeTime: String) {
        this.openTime = openTime
        this.closeTime = closeTime
        this.day = ""
    }

}