package com.example.android.shoppingbasket

import android.content.Context
import android.content.res.Resources
import android.support.annotation.StyleRes
import android.util.AttributeSet
import android.widget.AutoCompleteTextView

class CustomAutoCompleteView @JvmOverloads constructor(
                                    context: Context?,
                                    attrs: AttributeSet? = null,
                                    defStyleAttr: Int = 0,
                                    defStyleRes: Int = 0
                            ) : android.support.v7.widget.AppCompatAutoCompleteTextView(context, attrs) {



    override fun performFiltering(text: CharSequence?, keyCode: Int ) {
        var filterText = ""
        super.performFiltering(filterText, keyCode)
    }

    override fun replaceText(text: CharSequence?) {
        super.replaceText(text)
    }
}