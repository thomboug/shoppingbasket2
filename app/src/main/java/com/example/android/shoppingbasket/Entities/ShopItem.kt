package com.example.android.shoppingbasket.Entities

import com.example.android.shoppingbasket.Entities.enums.CategoryEnum
import java.io.Serializable

/**
 * Created by Erik on 2017. 10. 08..
 */

class ShopItem(var name: String) : Serializable {
    var id: Int = 0
    var barCode: String? = null
    var category: String? = null
    var price: Float = 0.toFloat()
    var shop: Store? = null
    var quantity :String = "Unit"
    var number: Int = 1


    fun ShopItem(name: String, barcode: String? = null, category: String = CategoryEnum.OTHER.name, price: Float = 0.0F, shop: Store? = null, number: Int = 1) {
        this.name = name
        this.barCode = barcode
        this.category = category
        this.price = price
        this.shop = shop
        this.number = number
    }

    fun ShopItem(name: String) {
        this.name = name
        this.barCode = null
        this.category = ""
        this.price = 0f
        this.shop = null
        this.number = 1
    }

    override fun toString(): String {
        return name
    }

    override fun equals(o: Any?): Boolean {
        if (o !is ShopItem) {
            return false
        }
        val other = o as ShopItem?
        return name.equals(other!!.name) && id == other!!.id
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

}
