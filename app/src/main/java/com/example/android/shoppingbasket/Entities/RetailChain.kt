package com.example.android.shoppingbasket.Entities

import com.example.android.shoppingbasket.Entities.enums.Retail
import java.io.Serializable

class RetailChain(var name: Retail = Retail.NON): Serializable {
    var id: Int = 0
    var url: String
    var homeDelivery: String

    init {
        when (name) {
            Retail.TESCO -> {
                id = 0
                url = "https://tesco.hu/"
                homeDelivery = "https://bevasarlas.tesco.hu/groceries/"
            }
            Retail.AUCHAN -> {
               id = 1
               url = "https://www.auchan.hu/"
               homeDelivery = "https://online.auchan.hu/"
            }
            Retail.ALDI -> {
                id = 2
               url = "https://www.aldi.hu/"
               homeDelivery = "None"
            }
            Retail.CBA -> {
                id = 3
                url = "https://www.cba.hu/"
                homeDelivery = "https://online.prima.hu/"
            }
            Retail.LIDL -> {
                id = 4
                url = "https://www.lidl.hu/"
                homeDelivery = "None"
            }
            Retail.PENNY -> {
                id = 5
                url = "https://www.penny.hu/"
                homeDelivery = "None"
            }
            Retail.SPAR -> {
                id = 6
                url = "https://www.spar.hu/hu_HU.html"
                homeDelivery = "None"
            }
            Retail.NON -> {
                id = 7
                url = "None"
                homeDelivery = "None"
            }
        }
    }

    override fun toString(): String {
        return name.value
    }

}