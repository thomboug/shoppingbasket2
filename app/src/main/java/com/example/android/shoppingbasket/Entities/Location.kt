package com.example.android.shoppingbasket.Entities

import android.location.Address
import java.io.Serializable

class Location (val address: String): Serializable {
    var id: Int = 0
    var postCode: String? = null;

    override fun toString(): String {
        return address
    }
}