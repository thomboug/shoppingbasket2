package com.example.android.shoppingbasket.Entities

import com.example.android.shoppingbasket.Entities.enums.Retail
import java.io.Serializable

class Store: Serializable {
    var id: Int = 0
    var name: String? = null
    var openingHours: OpeningHours? = null
    var location: Location? = null
    var retailChain: RetailChain? = RetailChain(Retail.NON)

    override fun toString(): String {
        return name!!
    }

}