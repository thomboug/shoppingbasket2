package com.example.android.shoppingbasket.Entities.enums

enum class Retail(val value: String) {
    TESCO("Tesco"),
    SPAR("Spar"),
    CBA("CBA"),
    AUCHAN("Auchan"),
    LIDL("Lidl"),
    ALDI("Aldi"),
    PENNY("Penny Market"),
    NON("Other")
}