package com.example.android.shoppingbasket

import android.content.Intent
import android.net.Uri
import android.support.v4.content.ContextCompat.startActivity
import android.support.v4.content.FileProvider
import com.ShoppingBasket.Data.DBHandler
import com.example.android.shoppingbasket.Entities.Coupon
import com.example.android.shoppingbasket.Entities.ShopItem
import com.opencsv.CSVParserBuilder
import com.opencsv.CSVWriter
import java.io.File
import java.io.FileWriter
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.ArrayList

class DataExporter : FileProvider() {

    fun export(shoppingDb: DBHandler): File {
        val baseDir: String = android.os.Environment.getExternalStorageDirectory().absolutePath
        val fileName = "Export.csv"
        val filePath: String = baseDir + File.separator + fileName
        var f = File(filePath)
        var temp : FileWriter
        var writer : CSVWriter
        var builder = CSVParserBuilder()
        builder.withSeparator(';')
        writer = if(f.exists()&&!f.isDirectory) {
            CSVWriter(FileWriter(filePath,true))
        }
        else {
            temp = FileWriter(f)
            CSVWriter(temp)
        }

        var data = arrayOf("Shop Items")
        writer.writeNext(data)
        data = arrayOf("name","barcode","price","category","quantityUnit","quantity","shop")
        writer.writeNext(data)
        val shopItems = shoppingDb.getAllEntries(ShopItem("")) as ArrayList<ShopItem>
        if(shopItems != null) {
            for (item in shopItems!!) {
                data = arrayOf(item.name, if (item.barCode != null) item.barCode!! else "", item.price.toString(), if (item.category != null) item.category!! else "",
                        item.quantity, item.number.toString(), if(item.shop != null) item.shop!!.toString() else "")
                writer.writeNext(data)
            }
        }
        data = arrayOf("Coupons")
        writer.writeNext(data)
        data = arrayOf("name","deductPercent","forCategory","ifMinimumSpending","forRetail")
        writer.writeNext(data)
        val couponList = shoppingDb.getAllEntries(Coupon()) as ArrayList<Coupon>
        for(coupon in couponList) {
            data = arrayOf(coupon.name!!, coupon.deductPercent.toString(), if(coupon.forCategory != null) coupon.forCategory!! else "", coupon.ifMinimumSpending.toString())
            writer.writeNext(data)
        }
        writer.close()

        return f
    }
}