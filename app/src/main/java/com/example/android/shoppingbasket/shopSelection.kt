package com.example.android.shoppingbasket

import android.app.Activity
import android.content.Intent
import android.database.Cursor
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.ShoppingBasket.Data.DBHandler
import com.example.android.shoppingbasket.Entities.*
import com.example.android.shoppingbasket.Entities.enums.Retail
import java.io.Serializable
import java.util.ArrayList

class shopSelection : AppCompatActivity() {

    var shoppingDb: DBHandler = DBHandler(this)
    var selectedRetail: RetailChain = RetailChain(Retail.TESCO)

    private lateinit var homeBTN: ImageButton
    private lateinit var saveBTN: RelativeLayout
    private lateinit var nameEd: EditText
    private lateinit var addressEd: EditText
    private lateinit var postCodeEd: EditText
    private lateinit var weekdayOpeningEd: EditText
    private lateinit var weekdayClosingEd: EditText
    private lateinit var weekendOpeningEd: EditText
    private lateinit var weekendClosingEd: EditText
    private lateinit var retailSpinner: Spinner
    lateinit var retailAdapter: ArrayAdapter<RetailChain>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shop_selection)

        var listShopItems = shoppingDb.getAllEntries(ShopItem("")) as ArrayList<ShopItem>

        if(shoppingDb.getEntry(1, RetailChain()) == null ) {
            val retails = Retail.values()
            for(retail in retails) {
                shoppingDb.createEntry(RetailChain(retail))
            }
        }

        homeBTN = findViewById(R.id.homeBTN)
        homeBTN.setOnClickListener {
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }
        saveBTN = findViewById(R.id.saveBtn)
        nameEd = findViewById(R.id.nameEd)
        addressEd = findViewById(R.id.addressEd)
        postCodeEd = findViewById(R.id.postCodeEd)
        weekdayOpeningEd = findViewById(R.id.weekdayOpening)
        weekdayClosingEd = findViewById(R.id.weekdayClosing)
        weekendOpeningEd = findViewById(R.id.weekendOpening)
        weekendClosingEd = findViewById(R.id.weekendClosing)
        retailSpinner = findViewById(R.id.retailSpinner)

        var retailList: ArrayList<RetailChain> = ArrayList()
        for (retail in Retail.values()) {
            retailList.add(RetailChain(retail))
        }
        retailAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, retailList)
        retailAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        retailSpinner!!.adapter = retailAdapter

        class retailSpinnerActivity : Activity(), AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                // An item was selected, retrieve the selected item
                selectedRetail = parent.getItemAtPosition(pos) as RetailChain
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Do Nothing
            }
        }
        retailSpinner!!.onItemSelectedListener = retailSpinnerActivity()


        var counter :TextView = findViewById<View>(R.id.Counter) as TextView
        counter.text = "${listShopItems.size} items"

        saveBTN!!.setOnClickListener{
            val temp = Store()
            temp.name = nameEd!!.text.toString()
            temp.location = Location(addressEd?.text.toString())
            if (temp.location?.address != null) temp.location!!.postCode = postCodeEd?.text.toString()
            temp.openingHours?.OpeningHours(weekdayOpeningEd?.text.toString(), weekdayClosingEd?.text.toString())
            if(selectedRetail != null) {
                temp.retailChain = selectedRetail
            }
            temp.id = shoppingDb.createEntry(temp)
            val toast = Toast.makeText(applicationContext,
                    "Shop added!", Toast.LENGTH_SHORT)
            toast.show()
            var data = Intent()
            var bundle = Bundle()
            bundle.putSerializable("shop", temp as Serializable)
            data.putExtras(bundle)
            setResult(Activity.RESULT_OK, data)
            finish()
        }
    }

}
