package com.example.android.shoppingbasket

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.ArrayAdapter
import com.example.android.shoppingbasket.Entities.Cart
import com.example.android.shoppingbasket.Entities.ShopItem
import java.util.*

class AutoCompleteTextChangedListener(private val mContext: Context) : TextWatcher {
    val TAG = "TextChangedListener.kt"
    private var cart: Cart = Cart.getCart()
    var afterTextChanged: String? = null
    var beforeTextChanged: String? = null


    override fun afterTextChanged(editable: Editable?) {
        afterTextChanged = editable.toString()
    }

    override fun beforeTextChanged(userInput: CharSequence?, start: Int, before: Int, count: Int) {
        beforeTextChanged = userInput.toString();
    }

    override fun onTextChanged(userInput: CharSequence?, start: Int, before: Int, count: Int) {
        Log.e(TAG, "Userinput: $userInput")
        if (mContext is BasketList){
            val stringResult : ArrayList<String> = ArrayList()
            val result = mContext.getItemsFromDb(userInput.toString())
            result.forEach { stringResult.add(it.name!!) }
            mContext.searchItems = stringResult
            mContext.searchAdapter.notifyDataSetChanged()
            mContext.searchAdapter = ArrayAdapter(mContext, R.layout.autocomplete_item, mContext.searchItems)
            mContext.search.setAdapter(mContext.searchAdapter)
        }
    }
}