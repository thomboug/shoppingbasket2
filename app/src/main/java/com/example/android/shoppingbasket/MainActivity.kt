package com.example.android.shoppingbasket

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.view.View
import android.widget.*
import com.ShoppingBasket.Data.DBHandler
import com.example.android.shoppingbasket.Entities.Cart
import com.example.android.shoppingbasket.Entities.ShopItem
import com.example.android.shoppingbasket.Entities.enums.CategoryEnum
import com.example.android.shoppingbasket.Entities.enums.CategoryEnum.Companion.fromValue
import java.time.Instant
import java.time.format.DateTimeFormatter

import java.util.ArrayList
import android.content.pm.PackageManager
import android.support.annotation.NonNull
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.support.v4.app.ActivityCompat
import android.content.DialogInterface
import android.os.Build
import android.annotation.TargetApi
import android.content.Context
import android.os.Environment
import android.provider.LiveFolders.INTENT
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.os.Environment.getExternalStorageDirectory
import android.support.v4.content.FileProvider
import java.io.File


class MainActivity : AppCompatActivity() {
    private val PERMISSION_REQUEST_CODE = 200

    private lateinit var counter : TextView
    private var cartItems: ArrayList<ShopItem>? = null
    private var shopItems: ArrayList<ShopItem>? = null
    private var cart: Cart = Cart
    private lateinit var drawerLayout: DrawerLayout
    private var shoppingDb: DBHandler = DBHandler(this)
    private lateinit var categoryListView: ListView
    private lateinit var categoryAdapter: ArrayAdapter<String>
    private lateinit var selectedCategory: CategoryEnum

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<View>(R.id.app_bar_id) as android.support.v7.widget.Toolbar
        setSupportActionBar(toolbar)
        counter = findViewById<View>(R.id.Counter) as TextView

        if (checkPermission()) {
            requestPermissionAndContinue()
        }

        drawerLayout = findViewById(R.id.drawerLayout)
        drawerLayout.setScrimColor(Color.TRANSPARENT)
//      Functionality to slide the main view under the drawer, if the drawer is the same size as the original activity it creates an illusion of a new view, no need for it here, saving for future use
//
//        val content : RelativeLayout = findViewById(R.id.mainActivity)
//        val actionBarDrawerToggle = object : ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close) {
//            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
//                super.onDrawerSlide(drawerView, slideOffset)
//                val slideX: Float = drawerView.width * slideOffset
//                content.translationX = slideX
//            }
//        }
//        drawerLayout.addDrawerListener(actionBarDrawerToggle)
        categoryListView = findViewById(R.id.categList)

        var categList: ArrayList<String> = ArrayList()
        for (cat in CategoryEnum.values()) {
            categList.add(cat.value)
        }
        categoryAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, categList)
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        categoryListView!!.adapter = categoryAdapter

        class categoryListActivity : Activity(), AdapterView.OnItemClickListener {
            override fun onItemClick(adapter: AdapterView<*>?, v: View?, position: Int, a: Long) {
                toItemsList(adapter!!.getItemAtPosition(position) as String)
            }

        }
        categoryListView.onItemClickListener = categoryListActivity()

        shopItems = shoppingDb.getAllEntries(ShopItem("")) as ArrayList<ShopItem>
        cart.dbHandler = shoppingDb
        cart.loadCart()
        cartItems = cart.items
        var size: Int = cartItems!!.size
        counter.text = "$size items"

    }

    override fun onResume() {
        super.onResume()
        cart.loadCart()
        cartItems = cart.items
        var size: Int = cartItems!!.size
        counter.text = "$size items"
    }

    override fun onDestroy() {
        super.onDestroy()
        shoppingDb.closeDB()
    }

    fun toItemsList(category: String) {
        val toItemList = Intent (this@MainActivity, itemList::class.java)
        val temp: ArrayList<ShopItem> = shoppingDb.getItemsForCategory(fromValue(category)!!) as ArrayList<ShopItem>
        toItemList.putExtra("itemList", temp)
        toItemList.putExtra("category", category)
        startActivity(toItemList)
    }

    fun exportData() {
        val dataExporter = DataExporter()
        val exportedFile = dataExporter.export(shoppingDb)
        val path = FileProvider.getUriForFile(this, "com.example.android.shoppingbasket.DataExporter" , exportedFile)

        val intent = Intent(Intent.ACTION_SEND).apply {
                type= "text/plain"
                putExtra(Intent.EXTRA_SUBJECT, "Export from Shopping Basket.")
                putExtra(Intent.EXTRA_TEXT, "Shopping basket data export from ${DateTimeFormatter.ISO_INSTANT.format(Instant.now())}")
                putExtra(Intent.EXTRA_STREAM, path)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }


    //        Redirecting to the correct activity on taping a button
    fun onButtonClick(view: View) {
        if (view.id == R.id.newItemBTN) {
            val toNewItem = Intent(this@MainActivity, newItem::class.java)
            startActivity(toNewItem)
        }
        if (view.id == R.id.basketBTN) {
            val toBasketList = Intent(this@MainActivity, BasketList::class.java)
            toBasketList.putExtra("sampleObject", shopItems)
            startActivity(toBasketList)
        }
        if (view.id == R.id.homeBTN) {

        }
        if (view.id == R.id.resetDb) {
            shoppingDb.resetDatabase()
            shopItems = shoppingDb.getAllEntries(ShopItem("")) as ArrayList<ShopItem>
            cart.dbHandler = shoppingDb
            cart.loadCart()
            cartItems = cart.items
            var size: Int = cartItems!!.size
            counter.text = "$size items"
        }
        if (view.id == R.id.exportBTN) {
            exportData()
        }
    }

    private fun checkPermission(): Boolean {

        return ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissionAndContinue() {
        if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE)) {
                val alertBuilder = AlertDialog.Builder(this)
                alertBuilder.setCancelable(true)
                alertBuilder.setTitle(getString(R.string.permission_necessary))
                alertBuilder.setMessage(R.string.storage_permission_is_encessary_to_wrote_event)
                alertBuilder.setPositiveButton(android.R.string.yes) { dialog, which -> ActivityCompat.requestPermissions(this@MainActivity, arrayOf(WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE), PERMISSION_REQUEST_CODE) }
                val alert = alertBuilder.create()
                alert.show()
                Log.e("", "permission denied, show dialog")
            } else {
                ActivityCompat.requestPermissions(this@MainActivity, arrayOf(WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE), PERMISSION_REQUEST_CODE)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (permissions.isNotEmpty() && grantResults.isNotEmpty()) {

                var flag = true
                for (i in grantResults.indices) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        flag = false
                    }
                }
                if (!flag) {
                    finish()
                }
            } else {
                finish()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}