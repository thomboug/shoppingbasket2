package com.example.android.shoppingbasket

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager

import com.google.zxing.integration.android.IntentIntegrator

import java.util.ArrayList

import android.os.Build
import android.support.annotation.RequiresApi
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.view.View
import android.widget.*
import com.ShoppingBasket.Data.DBHandler
import com.example.android.shoppingbasket.Entities.Cart
import com.example.android.shoppingbasket.Entities.Coupon
import com.example.android.shoppingbasket.Entities.ShopItem

class BasketList : AppCompatActivity() {
    private val SCAN_REQUEST = 0x0000c0de
    private val NEW_ITEM_REQUEST = 250
    private val COUPON_REQUEST = 500
    private val EDIT_ITEM_REQUEST = 750
    private val EDIT_COUPON_REQUEST = 100

    var shoppingDb: DBHandler = DBHandler(this)
    var searchItems: ArrayList<String> = ArrayList()
    lateinit var searchAdapter: ArrayAdapter<String>
    lateinit var search: CustomAutoCompleteView
    lateinit var counter :TextView

    private lateinit var moneyView: TextView
    private lateinit var scanBTN: Button
    private lateinit var submitBTN: Button
    private lateinit var homeBTN: ImageButton
    private lateinit var listView: ListView
    private lateinit var couponFAB: FloatingActionButton
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var couponListView: ListView
    private var cart: Cart = Cart.getCart()
    private lateinit var cartItems: ArrayList<ShopItem>
    private lateinit var shopItems: ArrayList<ShopItem>
    private var adapter: AdapterItem? = null
    lateinit var couponAdapter : CouponListItem

    @RequiresApi(Build.VERSION_CODES.CUPCAKE)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basket_list)

        val i = intent
        val extras = intent.extras

        if (extras != null) {
            shopItems = i.getSerializableExtra("sampleObject") as ArrayList<ShopItem>
        }
        drawerLayout = findViewById(R.id.drawerLayout)
        drawerLayout.setScrimColor(Color.TRANSPARENT)
        drawerLayout.setScrimColor(Color.TRANSPARENT)

        scanBTN = findViewById(R.id.barcodeScn)
        moneyView = findViewById(R.id.moneyTxt)
        submitBTN = findViewById(R.id.submit)
        homeBTN = findViewById(R.id.homeBTN)
        listView = findViewById(R.id.shoppingList)
        counter = findViewById(R.id.Counter)
        homeBTN.setOnClickListener { finish() }
        couponFAB = findViewById(R.id.couponFAB)
        couponFAB.setOnClickListener {
            val toCouponPicker = Intent(this, CouponPicker::class.java)
            startActivityForResult(toCouponPicker, COUPON_REQUEST)
        }
        search= findViewById(R.id.itemSearch)
        search.addTextChangedListener(AutoCompleteTextChangedListener(this))

        searchAdapter = ArrayAdapter(this, R.layout.autocomplete_item, searchItems)
        search.setAdapter(searchAdapter)
        couponListView = findViewById(R.id.couponList)
        couponAdapter = CouponListItem(cart.coupons as ArrayList<Coupon>, this)
        couponListView.adapter = couponAdapter
        couponAdapter!!.notifyDataSetChanged()

        scanBTN.setOnClickListener {
            val scanIntegrator = IntentIntegrator(this@BasketList)
            scanIntegrator.initiateScan(IntentIntegrator.PRODUCT_CODE_TYPES)
        }
        counter.text = "${cart.items!!.size} items"
        submitBTN.setOnClickListener {
            val chosenItem: ArrayList<ShopItem> = getItemsFromDb(search.text.toString())
            if (chosenItem.size != 1) {
                val toNewItem = Intent(this, newItem::class.java)
                toNewItem.putExtra("name", search.text.toString())
                startActivityForResult(toNewItem, NEW_ITEM_REQUEST)
            } else {
                cartItems.add(chosenItem[0])
                cart.putItemInCart(chosenItem[0].id)
                moneyView.requestFocus()
                displayPrice()
                counter.text = "${cart.items!!.size} items"
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(search.windowToken, 0)
                search.text.clear()
                adapter!!.notifyDataSetChanged()
            }
        }

        adapter = AdapterItem(cart.items as ArrayList<ShopItem>, this)
        listView.adapter = adapter
        adapter!!.notifyDataSetChanged()
        displayPrice()

    }

    override fun onResume() {
        super.onResume()
        cartItems = shoppingDb.getAllEntries(ShopItem("")) as ArrayList<ShopItem>
        adapter = AdapterItem(cart.items as ArrayList<ShopItem>, this)
        listView.adapter = adapter
        adapter!!.notifyDataSetChanged()
        displayPrice()
    }

    fun displayPrice() {
        moneyView.text = "$ ${cart.getFinalCartPrice()}"
    }

    fun editItem(id: Int) {
        val toItemSettings = Intent(this, newItem::class.java)
        toItemSettings.putExtra("editMode", true)
        val temp = shoppingDb.getEntry(id, ShopItem("")) as ShopItem?
        toItemSettings.putExtra("item", temp)
        startActivityForResult(toItemSettings, EDIT_ITEM_REQUEST)
    }

    fun editCoupon(id: Int) {
        val toCouponSettings = Intent(this, newCoupon::class.java)
        toCouponSettings.putExtra("editMode", true)
        val temp = shoppingDb.getEntry(id, Coupon()) as Coupon?
        toCouponSettings.putExtra("coupon", temp)
        startActivityForResult(toCouponSettings, EDIT_COUPON_REQUEST)
    }

    fun getItemsFromDb (searchTerm: String): ArrayList<ShopItem> {

        this.shopItems = shoppingDb.getAllEntries(ShopItem("")) as ArrayList<ShopItem>
        val result = ArrayList<ShopItem>()
        for(item in shopItems) {
            if (item.name.startsWith(searchTerm,true)) {
                result.add(item)
            }
        }
        return result
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        when(requestCode) {
            SCAN_REQUEST -> {
                val scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent)
                if (scanningResult != null) {
                    val resultItem = shoppingDb.getEntryByBarcode(scanningResult.contents)
                    if (resultItem != null) {
                        cart.putItemInCart(resultItem.id)
                        cart.loadCart()
                        adapter!!.notifyDataSetChanged()
                        counter.text = "${cart.items!!.size} items"
                        displayPrice()
                    } else {
                        val toNewItem = Intent(this, newItem::class.java)
                        toNewItem.putExtra("barCode", scanningResult.contents)
                        startActivityForResult(toNewItem, NEW_ITEM_REQUEST)
                    }
                } else {
                    val toast = Toast.makeText(applicationContext,
                            "No scan data received!", Toast.LENGTH_SHORT)
                    toast.show()
                }
            }
            NEW_ITEM_REQUEST -> {
                val result = intent!!.getSerializableExtra("item") as ShopItem
                cart.putItemInCart(result.id)
                cart.loadCart()
                adapter!!.notifyDataSetChanged()
                counter.text = "${cart.items!!.size} items"
                displayPrice()

                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(search.windowToken, 0)
                search.text.clear()
            }
            EDIT_ITEM_REQUEST -> {
                cart.loadCart()
                adapter!!.notifyDataSetChanged()
                displayPrice()

            }
            EDIT_COUPON_REQUEST -> {
                cart.loadCart()
                couponAdapter!!.notifyDataSetChanged()
                displayPrice()
            }
            COUPON_REQUEST -> {
                val result = intent!!.getSerializableExtra("coupon") as Coupon
                cart.putCouponInCart(result.id)
                cart.loadCart()
                couponAdapter.notifyDataSetChanged()
                displayPrice()
                val toast = Toast.makeText(applicationContext,
                        "Coupon applied!", Toast.LENGTH_LONG)
                toast.show()
            }
            else -> {
                val toast = Toast.makeText(applicationContext,
                        "Unidentified request code!", Toast.LENGTH_SHORT)
                toast.show()
            }
        }
    }
}
