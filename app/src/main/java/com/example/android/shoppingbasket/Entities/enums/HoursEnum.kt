package com.example.android.shoppingbasket.Entities.enums

enum class HoursEnum(val code: String) {
    ALL_DAY("0-24"),
    CLOSED("x"),
    NORMAL_BUSSINESS_HOURS("07-20")
}