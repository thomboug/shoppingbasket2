package com.example.android.shoppingbasket.Entities

import com.ShoppingBasket.Data.DBHandler
import java.util.*
import kotlin.collections.ArrayList

object Cart : Observable() {

    var items: ArrayList<ShopItem>? = ArrayList()
    var coupons: ArrayList<Coupon>? = ArrayList()
    var dbHandler: DBHandler? = null

    fun loadCart() {
        if (dbHandler != null) {
            items = dbHandler!!.getAllItemsFromCart() as ArrayList<ShopItem>
            coupons = dbHandler!!.getAllCouponsFromCart() as ArrayList<Coupon>
        }
    }

    fun saveCart() {
        if (dbHandler != null) {

        }
    }

    fun getCart(): Cart {
        return this
    }

    fun putItemInCart(id: Int) {
        var temp: ShopItem = dbHandler!!.getEntry(id, ShopItem("")) as ShopItem
        items!!.add(temp)
        dbHandler!!.putItemInCart(id)
        loadCart()
    }

    fun deleteItemFromCart(id: Int) {
        var temp: ShopItem = dbHandler!!.getEntry(id, ShopItem("")) as ShopItem
        items!!.remove(temp)
        dbHandler!!.deleteItemFromCart(id)
        loadCart()
    }

    fun putCouponInCart(id: Int) {
        var temp: Coupon = dbHandler!!.getEntry(id, Coupon()) as Coupon
        coupons!!.add(temp)
        dbHandler!!.putCouponInCart(id)
        loadCart()
    }

    fun deleteCouponFromCart(id: Int) {
        var temp: Coupon = dbHandler!!.getEntry(id, Coupon()) as Coupon
        coupons!!.remove(temp)
        dbHandler!!.deleteCouponFromCart(id)
        loadCart()
    }

    fun getFinalCartPrice() :Double {
        var sum = 0.toDouble()
        for (item in this!!.items!!) {
            sum += deductCoupon(item) * item.number
        }
        return Math.round(sum*100.0)/100.0
    }

    fun getOriginalCartPrice() :Double {
        var sum = 0.toFloat()
        for (item in this!!.items!!) {
            sum += item.price * item.number
        }
        return Math.round(sum*100.0)/100.0
    }

    fun deductCoupon(item: ShopItem): Double {
        var deductedPrice: Double = item.price.toDouble()
        if (coupons == null || coupons?.size == 0) {
            return deductedPrice
        }

        for (coupon in coupons!!) {
            if (getOriginalCartPrice().toInt() > coupon.ifMinimumSpending) {
                if (coupon.forCategory == null || coupon.forCategory == item.category) {
                    deductedPrice = Math.round(((deductedPrice * ((100 - coupon.deductPercent) / 100.0)) * 100.0)) / 100.0
                }
            }
        }
        return deductedPrice
    }


}