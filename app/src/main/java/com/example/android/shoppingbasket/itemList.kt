package com.example.android.shoppingbasket


import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.View
import android.widget.*
import com.ShoppingBasket.Data.DBHandler
import com.example.android.shoppingbasket.Entities.Cart
import com.example.android.shoppingbasket.Entities.Coupon
import com.example.android.shoppingbasket.Entities.ShopItem
import com.example.android.shoppingbasket.Entities.Store
import org.w3c.dom.Text
import java.io.Serializable

class itemList : AppCompatActivity() {

    var shoppingDb: DBHandler = DBHandler(this)
    private var cart: Cart = Cart.getCart()

    private lateinit var itemListView: ListView
    private lateinit var titleTxt: TextView
    private lateinit var homeBTN: ImageButton
    private lateinit var counter: TextView


    lateinit var itemAdapter: ArrayAdapter<ShopItem>
    lateinit var itemList: ArrayList<ShopItem>
    var selectedCategory: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_itemlist)

        val i = intent
        val extras = intent.extras
        if (extras != null) {
            itemList = i.getSerializableExtra("itemList") as ArrayList<ShopItem>
            selectedCategory = i.getStringExtra("category")
        }

        homeBTN = findViewById(R.id.homeBTN)
        homeBTN.setOnClickListener {
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }

        counter = findViewById(R.id.Counter)
        counter.text = "${cart.items!!.size} items"
        titleTxt = findViewById(R.id.Title)
        titleTxt.text = "$selectedCategory items:"

        itemListView = this.findViewById(R.id.itemList)
        itemAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, itemList)
        itemListView.adapter = itemAdapter

        class itemListActivity : Activity(), AdapterView.OnItemClickListener {
            override fun onItemClick(adapter: AdapterView<*>?, v: View?, position: Int, a: Long) {
                toEditItem(adapter!!.getItemAtPosition(position) as ShopItem)
            }
        }
        itemListView.onItemClickListener = itemListActivity()
    }

    fun toEditItem(selectedItem: ShopItem) {
        val toItemSettings = Intent(this, newItem::class.java)
        toItemSettings.putExtra("editMode", true)
        toItemSettings.putExtra("item", selectedItem)
        startActivity(toItemSettings)
    }
}
