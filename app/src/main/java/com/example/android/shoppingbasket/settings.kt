package com.example.android.shoppingbasket

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.ShoppingBasket.Data.DBHandler
import com.example.android.shoppingbasket.Entities.ShopItem

import com.google.zxing.integration.android.IntentIntegrator

import java.util.ArrayList

class settings : AppCompatActivity() {

    private var item = ShopItem("")
    internal lateinit var listShopItems: ArrayList<ShopItem>
    private var nameEd: EditText? = null
    private var barcodeEd: TextView? = null
    private var priceEd: EditText? = null
    private var categEd: TextView? = null
    private var shopEd: TextView? = null
    private var submitBTN: Button? = null
    private var scanBTN: Button? = null
    internal var position: Int = 0

    var shoppingDb: DBHandler = DBHandler(this)



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        val i = intent
        val extras = intent.extras

        if (extras != null) {
            listShopItems = i.getSerializableExtra("sampleObject") as ArrayList<ShopItem>
            position = i.getIntExtra("position", 0)
        }
        item = listShopItems[position]
        nameEd = findViewById(R.id.nameEd)
        nameEd!!.setOnClickListener { nameEd!!.setText("") }
        priceEd = findViewById(R.id.priceEd)
        priceEd!!.setOnClickListener { priceEd!!.setText(0) }
        categEd = findViewById(R.id.categEd)
        shopEd = findViewById(R.id.shopEd)
        barcodeEd = findViewById<View>(R.id.barcodeEd) as TextView
        barcodeEd!!.text = item.barCode

        nameEd!!.setText(item.name)
        priceEd!!.setText(item.price.toString())
        categEd!!.setText(item.category)
        shopEd!!.setText(item.shop?.name)

        submitBTN = findViewById<View>(R.id.submitBTN) as Button
        submitBTN!!.setOnClickListener {
            val temp = ShopItem("")
            temp.name = nameEd!!.text.toString()
            temp.barCode = barcodeEd!!.text.toString()
            temp.price = priceEd!!.text.toString().toFloat()
            temp.category = categEd!!.text.toString()
            item = temp
            listShopItems[position] = temp

//            val toBasket = Intent(this@settings, BasketList::class.java)
//            toBasket.putExtra("sampleObject", listShopItems)

            if (shoppingDb.getEntry(item.id, ShopItem("")) == null) {
                temp.id = shoppingDb.createEntry(item)
            } else {
                shoppingDb.updateEntry(item)
            }
            finish()
        }

        scanBTN = findViewById<View>(R.id.ScanBTN) as Button
        scanBTN!!.setOnClickListener {
            val scanIntegrator = IntentIntegrator(this@settings)
            scanIntegrator.initiateScan()
        }

    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        val scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent)
        if (scanningResult != null) {
            val barcode = scanningResult.contents
            val result: ShopItem? = shoppingDb.getEntryByBarcode(barcode)
            if (result != null) {
                nameEd!!.setText(result.name)
                barcodeEd!!.setText(result.barCode)
                priceEd!!.setText(result.price.toString())
                categEd!!.setText(result.category)
                return
            }
            barcodeEd!!.text = barcode
            //            String scanFormat = scanningResult.getFormatName();
            //            formatTxt.setText("FORMAT: " + scanFormat);
            //            contentTxt.setText("CONTENT: " + barCode);
            //            ShopItem temp = new ShopItem();
            //            temp.ShopItem("", barCode, "", 0, "");
            //            Intent toSettings = new Intent(BasketList.this, settings.class);
            //            toSettings.putExtra("sampleObject",temp);
            //            startActivity(toSettings);
        } else {
            val toast = Toast.makeText(applicationContext,
                    "No scan data received!", Toast.LENGTH_SHORT)
            toast.show()
        }
    }

}
