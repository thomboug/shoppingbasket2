package com.example.android.shoppingbasket

import android.content.Intent
import android.os.Bundle
import com.google.zxing.integration.android.IntentIntegrator
import android.app.Activity
import android.view.View
import android.widget.*
import com.ShoppingBasket.Data.DBHandler
import com.example.android.shoppingbasket.Entities.Cart
import com.example.android.shoppingbasket.Entities.ShopItem
import com.example.android.shoppingbasket.Entities.Store
import com.example.android.shoppingbasket.Entities.enums.CategoryEnum
import com.example.android.shoppingbasket.Entities.enums.CategoryEnum.Companion.fromValue
import java.io.Serializable
import java.util.ArrayList

class newItem : Activity() {

    private val SCAN_REQUEST = 0x0000c0de
    private val SHOP_REQUEST = 250

    private var listShopItems: ArrayList<ShopItem>? = null
    var shoppingDb: DBHandler = DBHandler(this)
    var shop: Store? = null
    var selectedCategory: String? = CategoryEnum.OTHER.toString()
    var selectedShop: Store? = null
    lateinit var shopAdapter : ArrayAdapter<Store>

    private var barCode: String? = null
    private var scanBTN: Button? = null
    private var saveBTN: RelativeLayout? = null
    private var nameEd: EditText? = null
    private var barcodeEd: TextView? = null
    private var priceEd: EditText? = null
    private var quantityEd: EditText? = null
    private var categSpinner: Spinner? = null
    private var shopSpinner: Spinner? = null
    private var shopBTN: Button? = null
    private var counter :TextView? = null
    private lateinit var homeBTN: ImageButton

    private var editMode: Boolean = false
    private var editId: Int = -1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_item)
        listShopItems = shoppingDb.getAllEntries(ShopItem("")) as ArrayList<ShopItem>

        scanBTN = findViewById(R.id.scan_button)
        saveBTN = findViewById(R.id.saveBtn)
        shopBTN = findViewById(R.id.shop_button)
        homeBTN = findViewById<View>(R.id.homeBTN) as ImageButton
        homeBTN.setOnClickListener {
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }

        nameEd = findViewById(R.id.nameEd)
        barcodeEd = findViewById(R.id.barcodeEd)
        priceEd = findViewById(R.id.priceEd)
        quantityEd = findViewById(R.id.quantityEd)
        shopSpinner = findViewById(R.id.shopEd)
        counter = findViewById(R.id.Counter)
        counter!!.text = "${Cart.items!!.size} items"

        var storeList: ArrayList<Store> = shoppingDb.getAllEntries(Store()) as ArrayList<Store>
        shopAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, storeList)
        shopAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        shopSpinner!!.adapter = shopAdapter

        class shopSpinnerActivity : Activity(), AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                // An item was selected, retrieve the selected item
                selectedShop = parent.getItemAtPosition(pos) as Store
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Do Nothing
            }
        }

        shopSpinner!!.onItemSelectedListener = shopSpinnerActivity()


        categSpinner = findViewById(R.id.categSpinner)
        ArrayAdapter.createFromResource(
                this,
                R.array.categories_array,
                android.R.layout.simple_spinner_item
        ).also { categAdapter ->
            // Specify the layout to use when the list of choices appears
            categAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            categSpinner!!.adapter = categAdapter
        }

        class CategSpinnerActivity : Activity(), AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                // An item was selected, retrieve the selected item
                selectedCategory = parent.getItemAtPosition(pos).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Do Nothing
            }
        }
        categSpinner!!.onItemSelectedListener = CategSpinnerActivity()

        scanBTN!!.setOnClickListener{
            val scanIntegrator = IntentIntegrator(this@newItem)
            scanIntegrator.initiateScan()
        }


        saveBTN!!.setOnClickListener{
            var temp = ShopItem("")
            if (editMode) {
                temp = shoppingDb.getEntry(editId, ShopItem("")) as ShopItem
            }
            temp.name = nameEd!!.text.toString()
            if (barcodeEd?.text.toString() != "") {
                temp.barCode = barcodeEd?.text.toString()
            }
            temp.price = priceEd!!.text.toString().toFloat()
            temp.quantity = quantityEd?.text.toString()
            if (selectedCategory != null && fromValue(selectedCategory.toString()) != null) {
                temp.category = fromValue(selectedCategory.toString())!!.name
            }
            if(selectedShop != null) {
                temp.shop = selectedShop
            }
            if(editMode) {
                temp.id = editId
                shoppingDb.updateEntry(temp)
                val toast = Toast.makeText(applicationContext,
                        "Item edited!", Toast.LENGTH_SHORT)
                toast.show()
            } else {
                temp.id = shoppingDb.createEntry(temp)
                val toast = Toast.makeText(applicationContext,
                        "Item added!", Toast.LENGTH_SHORT)
                toast.show()
            }

            val data = Intent()
            val bundle = Bundle()
            bundle.putSerializable("item", temp as Serializable)
            data.putExtras(bundle)
            setResult(RESULT_OK, data)
            finish()
        }

        val i = intent
        val extras = intent.extras
        val editedItem: ShopItem

        if (extras != null) {
            val incomingName = i.getStringExtra("name")
            nameEd!!.setText(incomingName)
            val incomingBarCode = i.getStringExtra("barCode")
            barcodeEd!!.text = incomingBarCode
            editMode = i.getBooleanExtra("editMode", false)

            if (editMode) {
                editedItem = i.getSerializableExtra("item") as ShopItem
                val titleTxt: TextView = findViewById(R.id.title)
                titleTxt.text = "Edit Item"
                nameEd!!.setText(editedItem.name)
                barcodeEd!!.text = editedItem.barCode
                priceEd!!.setText(editedItem.price.toString())
                quantityEd!!.setText(editedItem.quantity)
                shopSpinner!!.setSelection(shopAdapter.getPosition(editedItem.shop))
                categSpinner!!.setSelection(0)
                editId = editedItem.id
            }
        }

        shopBTN!!.setOnClickListener {
            val intent = Intent(this@newItem, shopSelection::class.java)
            startActivityForResult(intent, SHOP_REQUEST)
        }
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent) {
        when(requestCode) {
            SCAN_REQUEST -> {
                val scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent)
                if (scanningResult != null) {
                    barCode = scanningResult.contents
                    barcodeEd!!.text = barCode
                } else {
                    val toast = Toast.makeText(applicationContext,
                            "No scan data received!", Toast.LENGTH_SHORT)
                    toast.show()
                }
            }
            SHOP_REQUEST -> {
                val result: Store = intent.getSerializableExtra("shop") as Store
                shopAdapter.notifyDataSetChanged()
                shopSpinner!!.setSelection(shopAdapter.getPosition(result))
                selectedShop = result
                counter!!.text = "${Cart.items!!.size} items"
                shopAdapter.notifyDataSetChanged()
                var storeList: ArrayList<Store> = shoppingDb.getAllEntries(Store()) as ArrayList<Store>
                shopAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, storeList)
                shopSpinner!!.adapter = shopAdapter
            }
            else -> {
                val toast = Toast.makeText(applicationContext,
                        "Unidentified request code!", Toast.LENGTH_SHORT)
                toast.show()
            }
        }
    }

}
