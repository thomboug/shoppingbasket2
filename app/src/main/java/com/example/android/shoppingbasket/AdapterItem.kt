package com.example.android.shoppingbasket

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.android.shoppingbasket.Entities.Cart
import com.example.android.shoppingbasket.Entities.ShopItem
import com.example.android.shoppingbasket.Entities.enums.CategoryEnum

import java.util.ArrayList

/**
 * Created by Erik on 2017. 10. 08..
 */

class AdapterItem(_Cart_items: ArrayList<ShopItem>, context: Context) : ArrayAdapter<ShopItem>(context, R.layout.row_item, _Cart_items), View.OnClickListener {
    private val activity: Activity? = null
    private var cartItems: ArrayList<ShopItem>? = null
    private var cart = Cart.getCart()
    internal var mContext: Context = context

    private var lastPosition = -1

    class ViewHolder {
        internal var txtName: TextView? = null
        internal var txtCat: TextView? = null
        internal var txtCost: TextView? = null
        internal var txtQuantityUnit: TextView? = null
        internal var editTextQuantity: EditText? = null
        internal var deleteBtn: ImageButton? = null
        internal var editBtn: ImageButton? = null
        internal var drawerLayout: DrawerLayout? = null

    }

    init {
        this.cartItems = _Cart_items
        inflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return cart.items!!.size
    }

    override fun getItem(position: Int): ShopItem? {
        return cart.items!![position]
    }

    fun setItem(position: Int, _Shop_item: ShopItem) {
        cart.items!![position] = _Shop_item
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onClick(v: View) {

        val position = v.tag as Int
        val `object` = getItem(position)
        val dataModel = `object`
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        // Get the data item for this position
        val dataModel = cart.items!![position]
        // Check if an existing view is being reused, otherwise inflate the view
        var viewHolder = ViewHolder() // view lookup cache stored in tag

        val result: View

//        if (convertView == null) {

        val inflater = LayoutInflater.from(context)
        convertView = inflater.inflate(R.layout.row_item, parent, false)
        viewHolder.drawerLayout = convertView!!.findViewById(R.id.drawerLayout)
        viewHolder.drawerLayout!!.setScrimColor(Color.TRANSPARENT)
        viewHolder.txtName = convertView.findViewById(R.id.name)

        viewHolder.txtCat = convertView.findViewById(R.id.cat)

        viewHolder.txtCost = convertView.findViewById(R.id.cost)
        viewHolder.txtCost!!.text = "${dataModel.price * dataModel.number}"

        viewHolder.txtQuantityUnit = convertView.findViewById(R.id.quantityUnitTv)
        viewHolder.txtQuantityUnit!!.text = cart.items!![position].quantity

        viewHolder.editTextQuantity = convertView.findViewById(R.id.quantityEd)
        viewHolder.editTextQuantity?.addTextChangedListener (object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(viewHolder.editTextQuantity!!.text.toString().isNotEmpty()) {
                    val quantityNumber = viewHolder.editTextQuantity!!.text.toString().toInt()
                    cart.items!![position].number = quantityNumber
                    viewHolder.txtCost!!.text = "${cart.items!![position].price * quantityNumber}"
                    (mContext as BasketList).displayPrice()
                    (mContext as BasketList).shoppingDb.updateEntry(cart.items!![position])
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

        viewHolder.deleteBtn = convertView.findViewById(R.id.deleteBtn)
        viewHolder.deleteBtn!!.setOnClickListener {
            if(cartItems!!.size >0) {
                cart.items!![position].number = 1
                cart.deleteItemFromCart(getItem(position)!!.id)
                this.notifyDataSetChanged()
                if(mContext is BasketList) {
                    (mContext as BasketList).displayPrice()
                    (mContext as BasketList).counter.text = "${cart.items!!.size} items"
                }
            }
        }

        viewHolder.editBtn = convertView.findViewById(R.id.editBtn)
        viewHolder.editBtn!!.setOnClickListener {
            if(cartItems!!.size > 0) {
                (mContext as BasketList).editItem(dataModel.id)
            }
        }

        viewHolder.editTextQuantity!!.setText(dataModel.number.toString())
        viewHolder.txtName!!.text = dataModel.name
        viewHolder.txtCat!!.text = CategoryEnum.valueOf(dataModel.category.toString()).value
        convertView.tag = viewHolder

//        } else {
//            viewHolder = convertView.tag as ViewHolder
//        }

        viewHolder.editTextQuantity!!.setText(dataModel.number.toString())
        viewHolder.txtName!!.text = dataModel.name
        viewHolder.txtCat!!.text = CategoryEnum.valueOf(dataModel.category.toString()).value
        lastPosition = position
        // Return the completed view to render on screen
        return convertView
    }

    companion object {
        private var inflater: LayoutInflater? = null
    }

}
