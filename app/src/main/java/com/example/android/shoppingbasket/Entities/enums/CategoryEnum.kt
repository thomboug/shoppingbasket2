package com.example.android.shoppingbasket.Entities.enums

enum class CategoryEnum(val value: String) {
    BEVERAGES("Beverages"),
    BAKERY("Bread and other bakery"),
    DAIRY("Dairy"),
    CANNED_GOODS("Canned and bottled goods"),
    BAKING_GOODS("Baking goods, flour, sugar etc"),
    FROZEN_FOODS("Frozen Foods"),
    MEAT("Meats"),
    PRODUCE("Produce; Fruits and vegetables"),
    HOUSEHOLD("Household items"),
    PAPER_GOODS("Paper goods"),
    PERSONAL_CARE("Personal care"),
    SPICES("Spices"),
    DELI("Delicatessen"),
    SNACKS("Snacks"),
    OTHER("Other");

    companion object {
        private val map = values().associateBy(CategoryEnum::value)
        fun fromValue(category: String) = map[category]
    }
}