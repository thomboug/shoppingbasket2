package com.example.android.shoppingbasket.Entities

import com.example.android.shoppingbasket.Entities.enums.CategoryEnum
import com.example.android.shoppingbasket.Entities.enums.Retail
import java.io.Serializable
import android.R.attr.name
import android.R.id



class Coupon: Serializable {
    var id: Int = 0
    //TODO: Watch id
    var name: String? = null
    var deductPercent: Int = 0
    var ifMinimumSpending: Int = 0
    var forCategory: String? = null
    var forRetailChain: RetailChain? = RetailChain(Retail.NON)

    override fun toString(): String {
        return name!!
    }

    override fun equals(o: Any?): Boolean {
        if (o !is Coupon) {
            return false
        }
        val other = o as Coupon?
        return name.equals(other!!.name) && id == other!!.id
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

}