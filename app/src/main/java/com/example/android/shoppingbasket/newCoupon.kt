package com.example.android.shoppingbasket

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.view.View
import android.widget.*
import com.ShoppingBasket.Data.DBHandler
import com.example.android.shoppingbasket.Entities.Cart
import com.example.android.shoppingbasket.Entities.Coupon
import com.example.android.shoppingbasket.Entities.RetailChain
import com.example.android.shoppingbasket.Entities.enums.CategoryEnum
import com.example.android.shoppingbasket.Entities.enums.Retail

import kotlinx.android.synthetic.main.activity_new_coupon.*
import java.io.Serializable
import java.util.ArrayList

class newCoupon : AppCompatActivity() {

    private var cart: Cart = Cart.getCart()
    private var shoppingDb: DBHandler = DBHandler(this)

    var selectedCategory: String? = ""
    var selectedRetail: RetailChain = RetailChain(Retail.NON)
    lateinit var retailAdapter: ArrayAdapter<RetailChain>

    private lateinit var homeBTN: ImageButton
    private lateinit var counter : TextView
    private lateinit var saveBTN: RelativeLayout
    private lateinit var nameEd: EditText
    private lateinit var discountEd: EditText
    private lateinit var minCostEd: EditText
    private lateinit var categSpinner: Spinner
    private lateinit var retailSpinner: Spinner

    private var editMode: Boolean = false
    private var editId: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_coupon)

        homeBTN = findViewById(R.id.homeBTN)
        homeBTN.setOnClickListener {
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }

        counter = findViewById(R.id.Counter)
        counter.text = "${cart.items!!.size} items"

        nameEd = findViewById(R.id.nameEd)
        discountEd = findViewById(R.id.discountEd)
        minCostEd = findViewById(R.id.minCostEd)

        categSpinner = findViewById(R.id.categSpinner)
        ArrayAdapter.createFromResource(
                this,
                R.array.categories_array,
                android.R.layout.simple_spinner_item
        ).also { categAdapter ->
            categAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            categSpinner!!.adapter = categAdapter
        }

        class CategSpinnerActivity : Activity(), AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                selectedCategory = parent.getItemAtPosition(pos).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Do Nothing
            }
        }
        categSpinner!!.onItemSelectedListener = CategSpinnerActivity()

        retailSpinner = findViewById(R.id.retailSpinner)

        var retailList: ArrayList<RetailChain> = ArrayList()
        for (retail in Retail.values()) {
            retailList.add(RetailChain(retail))
        }
        retailAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, retailList)
        retailAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        retailSpinner!!.adapter = retailAdapter

        class retailSpinnerActivity : Activity(), AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                // An item was selected, retrieve the selected item
                selectedRetail = parent.getItemAtPosition(pos) as RetailChain
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Do Nothing
            }
        }
        retailSpinner!!.onItemSelectedListener = retailSpinnerActivity()

        saveBTN = findViewById(R.id.saveBtn)
        saveBTN.setOnClickListener {
            var temp = Coupon()
            if (editMode) {
                temp = shoppingDb.getEntry(editId, Coupon()) as Coupon
            }
            temp.name = nameEd!!.text.toString()
            val deductPercent: Int? = discountEd?.text.toString().toInt()
            if ( deductPercent != null && deductPercent < 100 && deductPercent > 0) {
                temp.deductPercent = deductPercent
            }
            temp.ifMinimumSpending = minCostEd.text.toString().toInt()

            if (selectedCategory != "" && selectedCategory != "No item selected") {
                temp.forCategory = CategoryEnum.fromValue(selectedCategory.toString())!!.name
            }
            temp.forRetailChain = selectedRetail

            if (editMode) {
                temp.id = editId
                shoppingDb.updateEntry(temp)
                val toast = Toast.makeText(applicationContext,
                        "Coupon edited!", Toast.LENGTH_SHORT)
                toast.show()
            } else {
                temp.id = shoppingDb.createEntry(temp)
                val toast = Toast.makeText(applicationContext,
                        "Coupon added!", Toast.LENGTH_SHORT)
                toast.show()
            }

            var data = Intent()
            var bundle = Bundle()
            bundle.putSerializable("coupon", temp as Serializable)
            data.putExtras(bundle)
            setResult(Activity.RESULT_OK, data)
            finish()
        }

        val i = intent
        val extras = intent.extras
        val editedCoupon: Coupon

        if (extras != null) {
            editMode = i.getBooleanExtra("editMode", false)

            if (editMode) {
                editedCoupon = i.getSerializableExtra("coupon") as Coupon
                val titleTxt: TextView = findViewById(R.id.title)
                titleTxt.text = "Edit Coupon"
                nameEd.setText(editedCoupon.name)
                discountEd.setText(editedCoupon.deductPercent.toString())
                minCostEd.setText(editedCoupon.ifMinimumSpending.toString())
                retailSpinner.setSelection(retailAdapter.getPosition(editedCoupon.forRetailChain))
                categSpinner.setSelection(0)
                editId = editedCoupon.id
            }
        }
    }

}
